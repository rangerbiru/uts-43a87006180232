/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.app.uts.npm232;

import java.text.DecimalFormat;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author bboyr
 */
@Entity
@Table(name = "Servis")
public class Servis {
    @Id
    @Column (name = "idservis")
    private String idServis;
    
    @Column (name = "namaservis")
    private String namaServis;
    
    @Column (name = "merkkendaraan")
    private String merkKendaraan;
    
    @Column (name = "pemilik")
    private String pemilik;
    
    @Column (name = "hargaservis")
    private DecimalFormat hargaServis;

    public Servis() {
    }

    public Servis(String idServis, String namaServis, String merkKendaraan, String pemilik, DecimalFormat hargaServis) {
        this.idServis = idServis;
        this.namaServis = namaServis;
        this.merkKendaraan = merkKendaraan;
        this.pemilik = pemilik;
        this.hargaServis = hargaServis;
    }

    public String getIdServis() {
        return idServis;
    }

    public void setIdServis(String idServis) {
        this.idServis = idServis;
    }

    public String getNamaServis() {
        return namaServis;
    }

    public void setNamaServis(String namaServis) {
        this.namaServis = namaServis;
    }

    public String getMerkKendaraan() {
        return merkKendaraan;
    }

    public void setMerkKendaraan(String merkKendaraan) {
        this.merkKendaraan = merkKendaraan;
    }

    public String getPemilik() {
        return pemilik;
    }

    public void setPemilik(String pemilik) {
        this.pemilik = pemilik;
    }

    public DecimalFormat getHargaServis() {
        return hargaServis;
    }

    public void setHargaServis(DecimalFormat hargaServis) {
        this.hargaServis = hargaServis;
    }

    
    
    
}
