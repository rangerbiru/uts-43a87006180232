package org.app.uts.npm232;

import org.app.uts.npm232.Servis;
import org.app.uts.npm232.ServisRepository;
import java.util.List;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
public class Uts43A87006180232Application {

public static void main(String[] args) {
	SpringApplication.run(Uts43A87006180232Application.class, args);
}

@Bean
public CommandLineRunner testServisRepository(ServisRepository repo){
    return a -> {
        List<Servis> list = repo.findAll();
        System.out.format("%-15s %-40s %-30s %-20s \n ","Id Servis","Nama Servis",
                    "Merk Kendaraan","Pemilik","Harga Servis");
            System.out.println("---------------------------------------------------------"
                    +"---------------------");
            list.forEach(c -> {
                System.out.format("%-15s %-40s %-30s %-20s \n ",
                c.getIdServis(),c.getNamaServis(),
                c.getMerkKendaraan(),c.getPemilik(),c.getHargaServis());
  });
    };
}

}
