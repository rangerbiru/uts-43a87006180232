/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.app.uts.npm232;

import org.app.uts.npm232.Servis;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author bboyr
 */
@Repository
public interface ServisRepository extends JpaRepository<Servis, String>{
    
}
